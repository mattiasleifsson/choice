package testDialogue;

import dialogue.*;
import player.Player;
import java.util.List;
import java.util.ArrayList;
import org.junit.*;
import static org.junit.Assert.*;

public class TestRequirement {
    private Requirement requirement;
    private Player player = new Player();

    @Test
    public void singleRequirementShouldTestPlayer() {
        requirement = new SingleRequirement("money", 3);

        assertEquals(false, requirement.test(player));
    }

    @Test
    public void multiRequirementShouldFailIfNotAllSingleReqsPass() {
        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new SingleRequirement("money", 3));
        requirements.add(new SingleRequirement("abc", 2));
        requirement = new MultiRequirement(requirements);
        player.give("money", 3);

        assertEquals(false, requirement.test(player));
    }

    @Test
    public void multiRequirementShouldPassIfAllSingleReqsPass() {
        List<Requirement> requirements = new ArrayList<>();
        requirements.add(new SingleRequirement("money", 3));
        requirements.add(new SingleRequirement("abc", 2));
        requirement = new MultiRequirement(requirements);
        player.give("money", 3);
        player.give("abc", 2);

        assertEquals(true, requirement.test(player));
    }

    @Test
    public void noRequirementShouldAlwaysPass() {
        assertEquals(true, new NoRequirement().test(player));
    }
}
