package testDialogue;

import dialogue.*;
import player.Player;
import java.util.List;
import java.util.ArrayList;
import org.junit.*;
import static org.junit.Assert.*;

public class TestEffect {
    private Effect effect;
    private Player player = new Player();

    @Test
    public void singleEffectShouldAffectPlayer() {
        effect = new SingleEffect("money", 3);

        effect.apply(player);
        assertEquals(true, player.has("money", 3));
    }

    @Test
    public void multiEffectShouldAffectPlayer() {
        List<Effect> effects = new ArrayList<>();
        effects.add(new SingleEffect("money", 3));
        effects.add(new SingleEffect("abc", 2));
        effect = new MultiEffect(effects);

        effect.apply(player);
        assertEquals(true, player.has("money", 3));
        assertEquals(true, player.has("abc", 2));
    }
}
