package testDialogue;

import org.junit.*;
import static org.junit.Assert.*;
import player.Player;
import dialogue.*;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class TestResponse {
    Map<String, Event> events = new HashMap<>();
    Player player = new Player();
    Response r1;
    Response r2;
    Response r3;

    public TestResponse() {
        List<String> nexts = new ArrayList<>();
        nexts.add("r2");
        nexts.add("r3");
        r1 = new Response("r1", "text", new NoRequirement(), new NoEffect(), nexts);
        r2 = new Response("r2", "text", new SingleRequirement("abc", 2), new NoEffect(), new ArrayList<>());
        r3 = new Response("r3", "text", new NoRequirement(), new NoEffect(), new ArrayList<>());

        events.put(r1.getId(), r1);
        events.put(r2.getId(), r2);
        events.put(r3.getId(), r3);
    }

    @Test
    public void nextShouldReturnNextEventThatCanOccur() {
        assertEquals(r3, r1.next(events, player));
    }

    @Test
    public void nextWillReturnFirstEventIfItIsValid() {
        player.give("abc", 2);

        assertEquals(r2, r1.next(events, player));
    }

    @Test(expected=RuntimeException.class)
    public void nextShouldReturnErrorIfNoValidNextEvent() {
        r3 = new Response("r3", "text", new SingleRequirement("a", 3), new NoEffect(), new ArrayList<>());
        events.put(r3.getId(), r3);

        r1.next(events, player);
    }
}
