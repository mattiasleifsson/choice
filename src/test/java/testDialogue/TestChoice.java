package testDialogue;

import org.junit.*;
import static org.junit.Assert.*;
import player.Player;
import dialogue.*;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class TestChoice {
    Map<String, Event> events = new HashMap<>();
    Player player = new Player();
    Choice c;
    Response r1;
    Response r2;
    
    public TestChoice() {
        List<String> choices = new ArrayList<>();
        choices.add("r1");
        choices.add("r2");
        c = new Choice("c", "text", new NoRequirement(), new NoEffect(), choices);
        r1 = new Response("r1", "text", new SingleRequirement("abc", 2), new NoEffect(), new ArrayList<>());
        r2 = new Response("r2", "text", new NoRequirement(), new NoEffect(), new ArrayList<>());

        events.put(c.getId(), c);
        events.put(r1.getId(), r1);
        events.put(r2.getId(), r2);
    }

    @Test
    public void choiceShouldReturnItselfIfNoValidChoice() {
        player.setChoice(3);
        assertEquals(c, c.next(events, player));
    }

    @Test
    public void choiceShouldReturnCorrectChoice() {
        player.setChoice(1);
        assertEquals(r2, c.next(events, player));
    }
    
}
