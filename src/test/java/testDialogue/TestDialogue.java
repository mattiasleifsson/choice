package testDialogue;

import org.junit.*;
import static org.junit.Assert.*;
import player.Player;
import dialogue.*;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

public class TestDialogue {
    Map<String, Event> events = new HashMap<>();
    Dialogue dialogue;
    Player player = new Player();

    public TestDialogue() {
        List<String> choices = new ArrayList<>();
        List<String> end = new ArrayList<>();
        end.add("end");
        choices.add("r1");
        choices.add("r2");
        Choice c = new Choice("c", "text", new NoRequirement(), new NoEffect(), choices);
        Response r1 = new Response("r1", "text", new SingleRequirement("abc", 2), new NoEffect(), end);
        Response r2 = new Response("r2", "text", new NoRequirement(), new NoEffect(), end);

        events.put(c.getId(), c);
        events.put(r1.getId(), r1);
        events.put(r2.getId(), r2);
    }

    @Test(expected=RuntimeException.class)
    public void dialogueMustHaveStartEvent() {
        dialogue = new Dialogue(events, player);
    }

    @Test
    public void dialogueIsOverOnlyWhenReachedEnd() {
        List<String> nexts = new ArrayList<>();
        nexts.add("c");
        events.put("start", new Response("start", "", new NoRequirement(), new NoEffect(), nexts));
        dialogue = new Dialogue(events, player);
        player.setChoice(1);
        assertEquals(false, dialogue.isOver());
        dialogue.next();
        assertEquals(false, dialogue.isOver());
        dialogue.next();
        assertEquals(false, dialogue.isOver());
        dialogue.next();
        assertEquals(true, dialogue.isOver());
    }
}

