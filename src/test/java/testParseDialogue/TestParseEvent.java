package testParseDialogue;

import org.junit.*;
import static org.junit.Assert.*;
import dialogue.*;
import parseDialogue.*;
import player.Player;
import java.util.Map;
import java.util.HashMap;

public class TestParseEvent {
    private Player player = new Player();

    @Test(expected=ParseException.class)
    public void eventMustHaveTags() {
        String s = "aefeafekal  ";
        Event e = ParseEvent.parse(s);
    }

    @Test(expected=ParseException.class)
    public void eventMustStartWithType() {
        String s = "/N a/T bla bla  ";
        Event e = ParseEvent.parse(s);
    }

    @Test
    public void eventWithChoiceTagShouldBeChoice() {
        String s = "   /C /N a/T bla bla  /I a";
        Event e = ParseEvent.parse(s);
        assertEquals(true, e instanceof Choice);
    }

    @Test
    public void eventWithResponseTagShouldBeResponse() {
        String s = "   /R /N a/T bla bla  /I a";
        Event e = ParseEvent.parse(s);
        assertEquals(true, e instanceof Response);
    }

    @Test
    public void eventShouldHaveCorrectId() {
        String s = "/C/N a/T bla bla  /I abc ";
        Event e = ParseEvent.parse(s);
        assertEquals("abc", e.getId());
    }

    @Test(expected=ParseException.class)
    public void eventMustHaveId() {
        String s = "/C/N a/T bla bla  ";
        Event e = ParseEvent.parse(s);
    }

    @Test(expected=ParseException.class)
    public void eventCanNotHaveEmptyId() {
        String s = "/C/N a/T bla bla  /I ";
        Event e = ParseEvent.parse(s);
    }

    @Test(expected=ParseException.class)
    public void eventCanNotHaveInvalidId() {
        String s = "/C/N a/T bla bla  /I this is not a valid id";
        Event e = ParseEvent.parse(s);
    }

    @Test
    public void eventShouldHaveCorrectText() {
        String s = "/C/N a/T bla bla  /I abc";
        Event e = ParseEvent.parse(s);
        assertEquals("bla bla", e.getText());
    }

    @Test(expected=ParseException.class)
    public void eventMustHaveNextsTag() {
        String s = "/C/T bla bla  /I abc";
        Event e = ParseEvent.parse(s);
    }

    @Test(expected=ParseException.class)
    public void eventCanNotHaveEmptyNextsTag() {
        String s = "/C/T bla bla  /N /I abc";
        Event e = ParseEvent.parse(s);
    }

    @Test
    public void eventShoudHaveCorrectNext() {
        Map<String, Event> events = new HashMap<>();
        Response r = new Response("b", "text", new NoRequirement(), new NoEffect(), null);
        events.put(r.getId(), r);
        String s = "/C/T bla bla  /Nb /I abc";
        Event e = ParseEvent.parse(s);
        assertEquals(r, e.next(events, player));
    }

    @Test
    public void eventShouldParseCorrectly() {
        String s = "  \n  /C/Qa=3,b = 4/I id \n/T this is the text  \n/N n, m";
        Event e = ParseEvent.parse(s);
        assertTrue("Event has wrong type", e instanceof Choice);
        assertEquals("Event has wrong id", "id", e.getId());
        assertEquals("Event has wrong text", "this is the text", e.getText());
        assertFalse("Event has wrong requirement", e.canOccur(player));
    }
}
