package testParseDialogue;

import org.junit.*;
import static org.junit.Assert.*;
import dialogue.*;
import parseDialogue.*;
import player.Player;
import java.util.Map;
import java.util.HashMap;
import java.io.InputStream;

public class TestParseDialogue {
    private Player player = new Player();

    @Test(expected=ParseException.class)
    public void thereHasToBeStartEvent() {
        String s = " /R /I a /N end";
        ParseDialogue.parse(s, player);
    }

    @Test(expected=ParseException.class) 
    public void noDuplicateEventIdsAllowed() {
        String s = " /R /I start /N a /C /I a /N end /R /I a /N end";
        ParseDialogue.parse(s, player);
    }

    @Test
    public void itShouldBePossibleToParseFromInputStream() {
        InputStream is = getClass().getClassLoader().getResourceAsStream("dialogue.txt");
        Dialogue d = ParseDialogue.parse(is, player);
        assertEquals("Start text", d.start());
        assertTrue(d.next().startsWith("How are you doing?"));
        assertEquals("fine", d.next());
        d.next();
        assertTrue(d.isOver());
    }
}
