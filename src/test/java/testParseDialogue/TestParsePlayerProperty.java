package testParseDialogue;

import org.junit.*;
import static org.junit.Assert.*;
import dialogue.*;
import parseDialogue.*;
import player.Player;

public class TestParsePlayerProperty {
    private Player player = new Player();
    
    @Test(expected=ParseException.class)
    public void exceptionWhenInvalidNumber() {
        String s = "abc=g";
        ParseEffect.parse(s);
    }

    @Test
    public void negativeNumbersShouldBeAllowed() {
        String s = "money=-3";
        ParseEffect.parse(s).apply(player);
        assertTrue(player.has("money", -3));
    }

    @Test
    public void longerNumberShouldBeAllowed() {
        String s = "money=789432";
        ParseEffect.parse(s).apply(player);
        assertTrue(player.has("money", 789432));
    }

    @Test
    public void spacesShouldBeAllowed() {
        String s = "    money= 789432";
        ParseEffect.parse(s).apply(player);
        assertTrue(player.has("money", 789432));
    }

    @Test(expected=ParseException.class)
    public void exceptionWhenNoPropertyName() {
        String s = " = 789432";
        ParseEffect.parse(s).apply(player);
    }

    @Test(expected=ParseException.class)
    public void exceptionWhenNoNumber() {
        String s = " abc=";
        ParseEffect.parse(s).apply(player);
    }

    @Test(expected=ParseException.class)
    public void exceptionWhenNoEquals() {
        String s = " abc 3";
        ParseEffect.parse(s).apply(player);
    }

    @Test(expected=ParseException.class)
    public void exceptionWhenSeveralEquals() {
        String s = " abc ==3";
        ParseEffect.parse(s).apply(player);
    }

    @Test
    public void multiplePropertiesShouldBeCommaSeparated() {
        String s = "abc=4, defgh = -1";
        ParseEffect.parse(s).apply(player);
        assertTrue(player.has("abc", 4));
        assertTrue(player.has("defgh", -1));
    }

    @Test
    public void newLinesShouldBeAllowed() {
        String s = "abc=4,\n defgh = -1";
        ParseEffect.parse(s).apply(player);
        assertTrue(player.has("abc", 4));
        assertTrue(player.has("defgh", -1));
    }

    @Test
    public void ManyPropertiesShouldBeAllowed() {
        String s = "a   = 123\n,  b=-3, c= 2,\n d = 2";
        ParseEffect.parse(s).apply(player);
        assertTrue(player.has("a", 123));
        assertTrue(player.has("b", -3));
        assertTrue(player.has("c", 2));
        assertTrue(player.has("d", 2));
    }
}
