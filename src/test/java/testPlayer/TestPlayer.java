package testPlayer;

import player.Player;
import org.junit.*;
import static org.junit.Assert.*;

public class TestPlayer {
    private Player player = new Player();

    @Test
    public void playerShouldHaveProperties() {
        player.give("money", 5);
    }

    @Test
    public void playerPropertyCheckShouldReturnTrueIfPlayerHasProperty() {
        player.give("money", 5);
        assertEquals(true, player.has("money", 5));
    }

    @Test
    public void propertyCheckForZeroShoudReturnTrueIfPlayerHasNoProperties() {
        assertEquals(true, player.has("abc", 0));
    }

    @Test
    public void propertyCheckForOneShouldReturnFalseIfPlayerHasNoProperties() {
        assertEquals(false, player.has("abc", 1));
    }
}
