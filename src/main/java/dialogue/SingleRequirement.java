package dialogue;

import player.*;

public class SingleRequirement implements Requirement {
    private String name;
    private int value;

    public SingleRequirement(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public SingleRequirement(PlayerProperty p) {
        this.name = p.getName();
        this.value = p.getValue();
    }

    public boolean test(Player player) {
        return player.has(name, value);
    }
}

