package dialogue;

import player.Player;
import java.util.List;
import java.util.Map;

public class Choice extends Event {
    private List<String> choices;

    public Choice(String id, String text, Requirement requirement, Effect effect, List<String> choices) {
        super(id, text, requirement, effect);
        this.choices = choices;
    }

    public String occur(Map<String, Event> events, Player player) {
        effect.apply(player);
        String s = getText();
        for (int i = 0; i < choices.size(); i++) {
            s += "\n" + i + ". " + events.get(choices.get(i)).getText();
        }
        return s;
    }

    public Event next(Map<String, Event> events, Player player) {
        if (player.getChoice() >= 0
            && player.getChoice() < choices.size()
            && events.get(choices.get(player.getChoice())).canOccur(player)) {
            return events.get(choices.get(player.getChoice()));
        } else {
            return this;
        }
    }
}
