package dialogue;

import player.Player;
import java.util.Map;

public class Dialogue {
    private Map<String, Event> events;
    private Player player;
    private Event current;

    public Dialogue(Map<String, Event> events, Player player) {
        this.events = events;
        this.player = player;
        if (!events.containsKey("start")) {
            throw new IllegalArgumentException("No start event");
        }
        current = events.get("start");
        this.events.put("end", new Response("end", "", new NoRequirement(), new NoEffect(), null));
    }

    public String start() {
        return current.getText();
    }

    public String next() {
        Event next = current.next(events, player);
        if (next == current) {
            return current.getText();
        } else {
            current = next;
            return current.occur(events, player);
        }
    }

    public boolean isOver() {
        return current.getId() == "end";
    }
}
