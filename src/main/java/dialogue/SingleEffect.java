package dialogue;

import player.*;

public class SingleEffect implements Effect {
    private String name;
    private int value;

    public SingleEffect(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public SingleEffect(PlayerProperty p) {
        this.name = p.getName();
        this.value = p.getValue();
    }

    public void apply(Player player) {
        player.give(name, value);
    }
}
