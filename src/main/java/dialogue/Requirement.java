package dialogue;

import player.Player;

public interface Requirement {
    boolean test(Player player);
}
