package dialogue;

import player.Player;
import java.util.Map;

public abstract class Event {
    private String id;
    private String text;
    private Requirement requirement;
    protected Effect effect;

    public Event(String id, String text, Requirement requirement, Effect effect) {
        this.id = id;
        this.text = text;
        this.requirement = requirement;
        this.effect = effect;
    }

    public String occur(Map<String, Event> events, Player player) {
        effect.apply(player);
        return getText();
    }

    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public boolean canOccur(Player player) {
        return requirement.test(player);
    }

    public abstract Event next(Map<String, Event> events, Player player);
}
