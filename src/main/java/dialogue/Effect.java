package dialogue;

import player.Player;

public interface Effect {
    void apply(Player player);
}
