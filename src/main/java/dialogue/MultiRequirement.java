package dialogue;

import player.Player;
import java.util.List;

public class MultiRequirement implements Requirement {
    private List<Requirement> requirements;

    public MultiRequirement(List<Requirement> requirements) {
        this.requirements = requirements;
    }

    public boolean test(Player player) {
        boolean passed = true;
        for (Requirement r : requirements) {
            passed = passed && r.test(player);
        }
        return passed;
    }
}


