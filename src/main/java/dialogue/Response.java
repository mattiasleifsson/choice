package dialogue;

import player.Player;
import java.util.List;
import java.util.Map;

public class Response extends Event {
    private List<String> nexts;

    public Response(String id, String text, Requirement requirement, Effect effect, List<String> nexts) {
        super(id, text, requirement, effect);
        this.nexts = nexts;
    }

    public Event next(Map<String, Event> events, Player player) {
        for (String s : nexts) {
            if (events.get(s).canOccur(player)) {
                return events.get(s);
            }
        }
        throw new RuntimeException("No valid next event");
    }
}
