package dialogue;

import player.Player;
import java.util.List;

public class MultiEffect implements Effect {
    private List<Effect> effects;

    public MultiEffect(List<Effect> effects) {
        this.effects = effects;
    }

    public void apply(Player player) {
        for (Effect e : effects) {
            e.apply(player);
        }
    }
}

