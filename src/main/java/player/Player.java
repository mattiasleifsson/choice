package player;

import java.util.Map;
import java.util.HashMap;

public class Player {

    private Map<String, Integer> properties;
    private int choice;

    public Player() {
        properties = new HashMap<>();
    }
    
    public void give(String property, int value) {
        properties.put(property, value);
    }

    public boolean has(String property, int value) {
        return properties.getOrDefault(property, 0) == value;
    }

    public void setChoice(int choice) {
        this.choice = choice;
    }

    public int getChoice() {
        return choice;
    }
}
