package player;

public class PlayerProperty {
    private String name;
    private int value;
    public PlayerProperty(String name, int value) {
        this.name = name;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public int getValue() {
        return value;
    }
}
