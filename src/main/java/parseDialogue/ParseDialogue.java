package parseDialogue;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;
import dialogue.*;
import player.Player;
import java.io.InputStream;

public class ParseDialogue {
    public static Dialogue parse(InputStream is, Player player) {
        Scanner scanner = new Scanner(is);
        StringBuilder sb = new StringBuilder();
        while (scanner.hasNextLine()) {
            sb.append(scanner.nextLine());
        }   
        return parse(sb.toString(), player);
    }

    public static Dialogue parse(String s, Player player) {
        Map<String, Event> events = new HashMap<>();
        for (String eventString : separateEvents(s)) {
            Event e = ParseEvent.parse(eventString);
            if (events.containsKey(e.getId())) {
                throw new ParseException("Two events with same id " + e.getId());
            }
            events.put(e.getId(), e);
        }

        if (!events.containsKey("start")) {
            throw new ParseException("No event with id start");
        }
        return new Dialogue(events, player);
    }

    private static List<String> separateEvents(String s) {
        List<String> eventStrings = new ArrayList<>();
        int i = 0;
        int j = 1;
        while (j < s.length()) {
            if (s.charAt(j-1) == '/' && (s.charAt(j) == 'C' || s.charAt(j) == 'R')) {
                eventStrings.add(s.substring(i, j-1));
                i = j-1;
            }
            j++;
        }
        eventStrings.add(s.substring(i));
        eventStrings.remove(0);
        return eventStrings;
    }   
}
