package parseDialogue;

import java.util.List;
import java.util.ArrayList;
import dialogue.*;
import player.PlayerProperty;

public class ParseEffect {
    public static Effect parse(String s) {
        List<Effect> effects = new ArrayList<>();
        List<PlayerProperty> properties = ParsePlayerProperty.parse(s);
        for (PlayerProperty p : properties) {
            effects.add(new SingleEffect(p));
        }
        return new MultiEffect(effects);
    }
}
