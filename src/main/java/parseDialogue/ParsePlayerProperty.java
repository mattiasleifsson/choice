package parseDialogue;

import java.util.List;
import java.util.ArrayList;
import dialogue.*;
import player.PlayerProperty;

public class ParsePlayerProperty {
    public static List<PlayerProperty> parse(String s) {
        List<PlayerProperty> properties = new ArrayList<>();
        String[] splits = s.replaceAll(" |\n", "").split(",");
        for (String split : splits) {
            properties.add(parseSingleProperty(split));
        }
        return properties;
    }

    private static PlayerProperty parseSingleProperty(String s) {
        String[] splits = s.split("=");

        if (splits.length != 2) {
            throw new ParseException("Invalid property " + s);
        }

        String name = splits[0];

        if (name.isEmpty()) {
            throw new ParseException("Property has empty name " + s);
        }

        int value = -1;

        try {
            value = Integer.parseInt(splits[1]);
        } catch (NumberFormatException e) {
            throw new ParseException("Invalid number " + splits[1] + " when parsing property");
        }

        return new PlayerProperty(name, value);
    }
}

