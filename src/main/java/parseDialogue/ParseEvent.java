package parseDialogue;

import dialogue.*;
import java.util.List;
import java.util.ArrayList;

public class ParseEvent {
    public static Event parse(String s) {
        boolean response = false;
        String id = null;
        String text = "";
        List<String> nexts = null;
        Requirement requirement = new NoRequirement();
        Effect effect = new NoEffect();
        
        String[] splits = s.replace("\n", "").split("/");

        if (splits.length < 2) {
            throw new ParseException("String is not a valid event");
        } else if (splits[1].startsWith("C")) {
            response = false;
        } else if (splits[1].startsWith("R")) {
            response = true;
        } else {
            throw new ParseException("Event must start with type tag (/C or /R)");
        }

        for (String split : splits) {
            if (split.startsWith("I")) {
                id = split.substring(1).trim();
            } else if (split.startsWith("T")) {
                text = split.substring(1).trim();
            } else if (split.startsWith("Q")) {
                requirement = ParseRequirement.parse(split.substring(1));
            } else if (split.startsWith("E")) {
                effect = ParseEffect.parse(split.substring(1));
            } else if (split.startsWith("N")) {
                nexts = parseNexts(split.substring(1));
            }
        }

        if (id == null) {
            throw new ParseException("Event is missing id tag (/I)");
        } else if (id.isEmpty()) {
            throw new ParseException("Id is empty for event");
        } else if (id.contains(" ")) {
            throw new ParseException("Id " + id + " is invalid");
        }

        if (nexts == null) {
            throw new ParseException("Event " + id + " has no tag for nexts (/N)");
        } else if (nexts.isEmpty()) {
            throw new ParseException("The nexts tag for event " + id + " is empty");
        } else if (nexts.contains("")) {
            throw new ParseException("Event " + id + " has an invalid next event");
        }

        if (response) {
            return new Response(id, text, requirement, effect, nexts);
        } else {
            return new Choice(id, text, requirement, effect, nexts);
        }
    }

    private static List<String> parseNexts(String s) {
        List<String> nexts = new ArrayList<>();
        for (String next : s.replace(" ", "").split(",")) {
            nexts.add(next);
        }
        return nexts;
    }
}
