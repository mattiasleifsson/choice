package parseDialogue;

public class ParseException extends RuntimeException {
    public ParseException(String errorMessage) {
        super(errorMessage);
    }
}
