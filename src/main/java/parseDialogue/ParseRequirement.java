package parseDialogue;

import java.util.List;
import java.util.ArrayList;
import dialogue.*;
import player.PlayerProperty;

public class ParseRequirement {
    public static Requirement parse(String s) {
        List<Requirement> requirements = new ArrayList<>();
        List<PlayerProperty> properties = ParsePlayerProperty.parse(s);
        for (PlayerProperty p : properties) {
            requirements.add(new SingleRequirement(p));
        }
        return new MultiRequirement(requirements);
    }
}
