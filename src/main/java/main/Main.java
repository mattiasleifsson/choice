package main;

import dialogue.*;
import parseDialogue.ParseDialogue;
import player.Player;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Player player = new Player();
        Dialogue d = ParseDialogue.parse(Main.class.getClassLoader().getResourceAsStream("dialogue.txt"), player);
        Scanner scanner = new Scanner(System.in);
        System.out.println(d.start());
        while (!d.isOver()) {
            player.setChoice(scanner.nextInt());
            System.out.println(d.next());
        }
    }
}
